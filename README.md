# Henoo Technical Test : Java Backend
Vincent Castronovo,
vincent.castronovo@telecomnancy.eu

# Commentaires

J'ai réalisé tout ce que j'ai pu dans le temps imparti, j'ai commencé après Noël et je m'arrête le dimanche 2 janvier, soit le 10e jour après le mail m'invitant à faire ce test (du 23 décembre).

Je tiens à préciser que je n'ai que très peu de notions en Spring Boot (seulement 2h de TP à Télécom Nancy à l'heure actuelle), j'ai donc du me former durant cette semaine.

J'ai passé un temps considérable à régler des erreurs (et testé différents tutos car certaines erreurs étaient tout simplement incompréhensibles), si bien que j'ai pu commencer à obtenir des résultats le 1er janvier. J'estime le temps que j'ai consacré à ce test (en comptant le temps à résoudre les erreurs au début) à environ 25 heures.
En ne comptant que le temps passé à travailler à partir du 1er, je pense avoir fait environ 12 heures.

# Ce que j'ai réalisé

* Mise en place du projet, création de la base de donnée (utilisation de MySQL)
* Inscription avec un mail et un mot de passe
* Connection avec mail et mot de passe
* Chiffrement du mot de passe dans la base de donnée
* Liste des utilisateurs
* Création du produit (enregistrement automatique du créateur et de la date de création dans la base de donnée)
* Liste des produits
* Gestion des accès aux pages selon la nécessité d'être connecté ou non
* Supprimer un produit si on en est le propriétaire (fonctionne dans les tests, mais je n'ai pas eu le temps de régler un bug qui l'empêche de fonctionner sur la version web)
* Modification des valeurs d'un produit (autorisé si l'utilisateur a créé ce produit) (fonctionne dans les tests mais je n'ai pas eu le temps de régler un bug qui l'empêche de fonctionner sur la version web)
* Liste des produits triés par date de création (je n'ai pas eu le temps de gérer les pages)
* Tests unitaires

# À noter

La base de donnée MySQL se nomme codejavadb, je pense que pour run l'application sur vos machines vous devez créer la votre et modifier le fichier application.properties