package com.vincent.henoo_test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class ProductRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository repoP;

    @Autowired
    private UserRepository repoU;

    @Test
    public void testCreateProduct() {
        User user = new User();
        user.setEmail("vcastronovo57@gmail.com");
        user.setPassword("tncy");
        user.setFirstName("Vincent");
        user.setLastName("Castronovo");

        User savedUser = repoU.save(user);

        Product product = new Product();
        product.setName("Guitare électrique");
        product.setPrice(200);
        product.setOwner(user);
        product.setCreation_date(new Date(System.currentTimeMillis()));

        Product savedProduct = repoP.save(product);

        Product existProduct = entityManager.find(Product.class, savedProduct.getId());
        //User existUser = entityManager.find(User.class, savedUser.getId());

        //assertThat(user.getEmail()).isEqualTo(existUser.getEmail());
        assertThat(product.getName()).isEqualTo(existProduct.getName());
        assertThat(product.getPrice()).isEqualTo(existProduct.getPrice());
        assertThat(product.getOwner()).isEqualTo(existProduct.getOwner());
        assertThat(product.getCreation_date()).isEqualTo(existProduct.getCreation_date());
    }

    @Test
    public void testFindByEmail() {
        String name = "Guitare électrique";
        Product product = repoP.findByName(name);

        assertThat(product.getName()).isEqualTo(name);
    }

    @Test
    public void testFindByOwner() {
        User user = new User();
        user.setEmail("vcastronovo57@gmail.com");
        user.setPassword("tncy");
        user.setFirstName("Vincent");
        user.setLastName("Castronovo");

        User savedUser = repoU.save(user);

        Product product = new Product();
        product.setName("Guitare électrique");
        product.setPrice(200);
        product.setOwner(user);
        product.setCreation_date(new Date(System.currentTimeMillis()));

        Product savedProduct = repoP.save(product);

        User user2 = new User();
        user2.setEmail("test@gmail.com");
        user2.setPassword("tncy");
        user2.setFirstName("test");
        user2.setLastName("google");

        User savedUser2 = repoU.save(user2);

        Product product2 = new Product();
        product2.setName("Guitare acoustique");
        product2.setPrice(150);
        product2.setOwner(user2);
        product2.setCreation_date(new Date(System.currentTimeMillis()));

        Product savedProduct2 = repoP.save(product2);

        List<Product> productsList = repoP.findByOwner(repoU.findByEmail("vcastronovo57@gmail.com"));

        assertThat(productsList.size()==1);
    }

    @Test
    public void testDeleteProductById() {
        User user = new User();
        user.setEmail("vcastronovo57@gmail.com");
        user.setPassword("tncy");
        user.setFirstName("Vincent");
        user.setLastName("Castronovo");

        User savedUser = repoU.save(user);

        Product product = new Product();
        product.setName("Guitare électrique");
        product.setPrice(200);
        product.setOwner(user);
        product.setCreation_date(new Date(System.currentTimeMillis()));

        Product savedProduct = repoP.save(product);

        User user2 = new User();
        user2.setEmail("test@gmail.com");
        user2.setPassword("tncy");
        user2.setFirstName("test");
        user2.setLastName("google");

        User savedUser2 = repoU.save(user2);

        Product product2 = new Product();
        product2.setName("Guitare acoustique");
        product2.setPrice(150);
        product2.setOwner(user2);
        product2.setCreation_date(new Date(System.currentTimeMillis()));

        Product savedProduct2 = repoP.save(product2);

        repoP.deleteProductById(product2.getId());

        assertThat(repoP.findAll().size()==1);
    }

}
