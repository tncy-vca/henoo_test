package com.vincent.henoo_test;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT p FROM Product p WHERE p.name = ?1")
    public Product findByName(String name);

    @Query("SELECT p FROM Product p WHERE p.owner = ?1")
    public List<Product> findByOwner(User owner);

    /*@Modifying
    @Query(value = "delete from Product p where p.id = ?1")
    public void deleteProductsById(Long id);*/

    @Modifying
    void deleteProductById(Long id);

/*
    @Modifying
    @Query("update Product p set p.name=:newName where p.id=:id")
    public void updateBookNameFromId(@Param("id") Long id, @Param("name") String newName);

    @Modifying
    @Query("update Product p set p.price=:newPrice where p.id=:id")
    public void updateBookPriceFromId(@Param("id") Long id, @Param("price") float newPrice);
*/
}
