package com.vincent.henoo_test;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private ProductRepository productRepo;
	
	@GetMapping("")
	public String viewHomePage() {
		return "index";
	}
	
	@GetMapping("/register")
	public String showRegistrationForm(Model model) {
		model.addAttribute("user", new User());
		
		return "signup_form";
	}
	
	@PostMapping("/process_register")
	public String processRegister(User user) {
		//Chiffrement du mot de passe dans la BD
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		
		userRepo.save(user);
		
		return "register_success";
	}
	
	@GetMapping("/users")
	public String listUsers(Model model) {
		//Liste de tous les utilisateurs
		List<User> listUsers = userRepo.findAll();
		model.addAttribute("listUsers", listUsers);
		
		return "users";
	}

	@GetMapping("/products")
	public String listProducts(Model model) {
		//Liste tous les produits
		List<Product> listProducts = productRepo.findAll();
		model.addAttribute("listProducts", listProducts);

		return "products";
	}

	@GetMapping("/products_by_date")
	public String listProductsByCreationDate(Model model) {
		//Liste de tous les produits, triés par date de création
		List<Product> listProducts = productRepo.findAll();
		Collections.sort(listProducts, new Comparator<Product>() {
			@Override
			public int compare(Product p1, Product p2) {
				return p1.getCreation_date().compareTo(p2.getCreation_date());
			}
		});

		model.addAttribute("listProductsByCreationDate", listProducts);

		return "products_by_date";
	}

	@GetMapping("/product_creation")
	public String showProductCreationForm(Model model) {
		model.addAttribute("product", new Product());

		return "product_creation";
	}

	@PostMapping("/process_product_creation")
	public String processProductCreation(Product product) {
		product.setCreation_date(new Date(System.currentTimeMillis()));

		//Obtenir l'utilisateur connecté
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		product.setOwner(userRepo.findByEmail(currentPrincipalName));

		productRepo.save(product);

		return "product_creation_success";
	}

	@GetMapping("/modify_product")
	public String listAllProducts(Model model) {
		List<Product> listProducts = productRepo.findAll();
		model.addAttribute("listAllProducts", listProducts);

		return "modify_product";
	}

	@GetMapping("/deleteProduct")
	public String deleteProduct(@RequestParam Long product_id) {
		Product product = productRepo.findById(product_id).get();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User connected = userRepo.findByEmail(currentPrincipalName);

		//Si l'utilisateur connecté a bien créé ce produit
		if (product.getOwner().getId()==connected.getId()) {
			productRepo.deleteProductById(product_id);
			System.out.println("PRODUCT DELETED");
		} else {
			System.out.println("Erreur : vous devez être le propriétaire du produit pour le supprimer");
		}
		return "redirect:/products";
	}

	@GetMapping("/showUpdateProduct")
	public ModelAndView showUpdateProduct(@RequestParam Long product_id) {
		Product prod = productRepo.findById(product_id).get();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User connected = userRepo.findByEmail(currentPrincipalName);

		//Si l'utilisateur connecté a bien créé ce produit
		if (prod.getOwner().getId()==connected.getId()) {
			ModelAndView mav = new ModelAndView("product_modification");
			Product product = productRepo.findById(product_id).get();
			mav.addObject("product", product);
			return mav;
		} else {
			System.out.println("Erreur : vous devez être le propriétaire du produit pour le modifier");
			return new ModelAndView("products");
		}
	}

	@PostMapping("/saveProduct")
	public String saveProduct(@ModelAttribute Product product) {
		productRepo.save(product);
		return "redirect:/products";
	}
}
