package com.vincent.henoo_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HenooTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HenooTestApplication.class, args);
    }

}
